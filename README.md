# spip2discourse

## Fonctionnement

Toute publication d'un article, dans **une** rubrique du site SPIP à configurer, crée un topic
avec le chapo et l'url publique de celui-ci
sur une instance discourse, dans **une** catégorie à configurer.

## Configuration

Dans `config/mes_options.php`:

```php
define('_SPIP2DISCOURSE_PUBLISHING_SECTION', <entier correspondant à la rubrique SPIP où est publié un article>)
define('_SPIP2DISCOURSE_DEFAULT_CATEGORY', <entier correspondant à la catégorie discourse où poster>);
define('_SPIP2DISCOURSE_API_KEY', '<API Key à créer sur l\'instance discourse>');
define('_SPIP2DISCOURSE_URL', '<url de l\'instance discourse>');
```
