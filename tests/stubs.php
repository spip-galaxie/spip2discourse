<?php

define('_LOG_CRITIQUE', '');

define('_LOG_ERREUR', '');

function include_spip(string $file): void {}

function recuperer_url(string $url, array $options): array { return []; }

function sql_fetsel(array|string $fields, array|string $tables, array|string $where): array { return []; }

function id_table_objet(string $type): string { return ''; }

function url_absolue(string $url): string { return ''; }

function generer_objet_url(int|string $id, string $entite, string $args, string $ancre, bool $public): string { return ''; }

function spip_log(string $messsage, string $name): void {}

function propre(string $text): string { return ''; }

function typo(string $text): string { return ''; }

function job_queue_add(string $function, string $description, array $arguments, string $file, bool $no_duplicate, int $time): int { return 0; }

function job_queue_link(int $job, array $object): void {}
