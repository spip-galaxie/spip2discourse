<?php

use SpipGalaxy\Plugin\Spip2Discourse\Discourse;

/**
 * Poste un topic sur une instance discourse.
 *
 * @param string $title      Titre du topic
 * @param string $raw        Corps du topic
 * @param ?string $createdAt 'Y-m-d H:i:s' formatted string or null (=now)
 */
function post2discourse(string $title, string $raw, ?string $createdAt = null): bool
{
    static $discourse = null;

    $response = false;
    try {
        if (is_null($discourse)) {
            include_spip('src/Discourse');
            $discourse = Discourse::fromConstants();
        }

        $createdAt = is_null($createdAt) ?
            new DateTimeImmutable() :
            DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $createdAt)
        ;

        $recuperer_url = $discourse
            ->setTopic($title, $raw, $createdAt)
            ->send()
        ;

        include_spip('inc/distant');
        $response = recuperer_url($recuperer_url['url'], $recuperer_url['options']);
        if (!$response || $response['status'] !== 200) {
            throw new DomainException('Mishap in posting to discourse', 1);
        }
    } catch (\Throwable $th) {
        $level = $th->getCode() === 0 ? _LOG_CRITIQUE : _LOG_ERREUR;
        spip_log($th->getMessage(), 'spip_to_discourse.' . $level);
        $response = false;
    }

    return !empty($response);
}

/**
 * Évènement de pré-édition.
 *
 * @param array{args:array{action?:string,objet?:string,spip_table_objet:string,id_objet:int},data?:array{date:string,statut:string}} $payload
 *
 * @return array{args:array{action?:string,objet?:string,spip_table_objet:string,id_objet:int},data?:array{date:string,statut?:string,titre?:string,chapo?:string,id_rubrique?:int}}
 */
function spip2discourse_pre_edition(array $payload): array
{
    // When
    if (
        defined('_SPIP2DISCOURSE_PUBLISHING_SECTION') &&
        isset($payload['args']['action']) &&
        isset($payload['args']['objet']) &&
        $payload['args']['action'] === 'instituer' &&
        $payload['args']['objet'] === 'article'
    ) {
        // Then
        $row = sql_fetsel(
            ['titre', 'chapo', 'id_rubrique'],
            [$payload['args']['spip_table_objet']],
            [id_table_objet($payload['args']['objet']) . '=' . $payload['args']['id_objet']],
        );

        $payload['data']['titre'] = $row['titre'];
        $payload['data']['chapo'] = $row['chapo'];
        $payload['data']['id_rubrique'] = (int) $row['id_rubrique'];
    }

    return $payload;
}

/**
 * Évènement de post-édition.
 *
 * @param array{args:array{action?:string,objet?:string,spip_table_objet:string,id_objet:int},data:array{date:string,statut?:string,titre?:string,chapo?:string,id_rubrique?:int}} $payload
 *
 * @return array{args:array{action?:string,objet?:string,spip_table_objet:string,id_objet:int},data:array{date:string,statut?:string,titre?:string,chapo?:string,id_rubrique?:int}}
 */
function spip2discourse_post_edition(array $payload): array
{
    // When
    if (
        defined('_SPIP2DISCOURSE_PUBLISHING_SECTION') &&
        isset($payload['args']['action']) &&
        isset($payload['args']['objet']) &&
        isset($payload['data']['id_rubrique']) &&
        isset($payload['data']['chapo']) &&
        isset($payload['data']['titre']) &&
        isset($payload['data']['statut']) &&
        $payload['args']['action'] === 'instituer' &&
        $payload['args']['objet'] === 'article' &&
        $payload['data']['statut'] === 'publie' &&
        $payload['data']['id_rubrique'] === constant('_SPIP2DISCOURSE_PUBLISHING_SECTION')
    ) {
        // Then
        $url = url_absolue(generer_objet_url(
            $payload['args']['id_objet'],
            $payload['args']['objet'],
            '',
            '',
            true,
        ));

        include_spip('inc/texte');
        $intro = propre($payload['data']['chapo']) . "\n\n[" . $payload['data']['titre'] . '](' . $url . ')';
        $title = typo($payload['data']['titre']);

        $job = job_queue_add(
            'post2discourse',
            'publication sur discourse',
            [
                $title,
                $intro,
                $payload['data']['date'],
            ],
            'spip2discourse_fonctions',
            false,
            (new DateTimeImmutable('+1 minute'))->getTimestamp(),
        );
        job_queue_link($job, [
            'objet' => $payload['args']['objet'],
            'id_objet' => $payload['args']['id_objet'],
        ]);
    }

    return $payload;
}
