# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Fixed

- fix: apply typo to title

## 1.0.0 - 2024-06-05

### Fixed

- fix: include fonctions file
- check pipeline API payload
- logfile does not accept numbers in its name

## 0.1.2 - 2024-05-27

### Added

- limited publishing by section

### Changed

- chapo instead of introduction as raw

## 0.1.1 - 2024-05-26

### Added

- Error management

### Fixed

- descriptif field for introduction

## 0.1.0 - 2024-05-25

### Added

- Initial commit
