<?php

namespace SpipGalaxy\Plugin\Spip2Discourse;

final class Discourse
{
    private string $payload = '';

    private string $url;
    private string $apiKey;
    private int $category;

    private function __construct(
        string $url,
        string $apiKey,
        int $category,
    ) {
        $this->url = $url;
        $this->apiKey = $apiKey;
        $this->category = $category;
    }

    /**
     * Instancie un connecteur discourse à partir de constantes.
     *
     * @throws \RuntimeException si une constante requise est manquante
     */
    public static function fromConstants(): static
    {
        if (!defined('_SPIP2DISCOURSE_DEFAULT_CATEGORY')) {
            throw new \RuntimeException('Constant _SPIP2DISCOURSE_DEFAULT_CATEGORY undefined.');
        }
        if (!defined('_SPIP2DISCOURSE_API_KEY')) {
            throw new \RuntimeException('Constant _SPIP2DISCOURSE_API_KEY undefined.');
        }
        if (!defined('_SPIP2DISCOURSE_URL')) {
            throw new \RuntimeException('Constant _SPIP2DISCOURSE_URL undefined.');
        }

        return new self(
            \constant('_SPIP2DISCOURSE_URL'),
            \constant('_SPIP2DISCOURSE_API_KEY'),
            \constant('_SPIP2DISCOURSE_DEFAULT_CATEGORY'),
        );
    }

    /**
     * Définit le topic à créer.
     *
     * @param string             $title
     * @param string             $raw
     * @param \DateTimeImmutable $createdAt
     *
     * @throws \UnexpectedValueException si $title ou $raw est vide
     */
    public function setTopic(string $title, string $raw, \DateTimeImmutable $createdAt): self
    {
        if (empty($title) || empty($raw)) {
            throw new \UnexpectedValueException('Title or raw cannot be empty.');
        }

        $payload = json_encode([
            'title' => $title,
            'raw' => $raw,
            'category' => $this->category,
            'created_at' => \preg_replace(
                ',\+00:00$,',
                'Z',
                $createdAt
                    ->setTimezone(new \DateTimeZone('UTC'))
                    ->format(\DateTimeInterface::RFC3339),
            ),
        ], \JSON_THROW_ON_ERROR);

        $this->payload = $payload;

        return $this;
    }

    /**
     * Fabrique les paramètres à passer à recuperer_url().
     *
     * @return array{url:string,options:array{methode:string,headers:string[],datas:string,version_http:string}}
     */
    public function send(): array
    {
        $headers = [
            'Content-Type' => 'application/json',
            'Api-Key' => $this->apiKey,
        ];

        return [
            'url' => $this->url . '/posts',
            'options' => [
                'methode' => 'POST',
                'headers' => $headers,
                'datas' => $this->payload,
                'version_http' => 'HTTP/1.1',
            ],
        ];
    }
}
